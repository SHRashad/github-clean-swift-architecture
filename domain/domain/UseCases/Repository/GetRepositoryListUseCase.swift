//
//  GetRepositoryListUseCase.swift
//  domain
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import RxSwift

public class GetRepositoryListUseCase : BaseUseCase {
    
    let repo: ProjectsRepositoryProtocol
    
    init(repo: ProjectsRepositoryProtocol) {
        self.repo = repo
    }
    
    public func execute(input: (paging: PagingEntity, body: FilterBodyEntity)) -> Observable<PaginatedEntity<RepositoryEntity>> {
        return repo.getRepositories(paging: input.0, body: input.1)
    }
}
