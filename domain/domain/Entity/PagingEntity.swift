//
//  PagingEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

public struct PagingEntity {
    public var pageNum: Int
    public var perPage: Int
    
    public var totalCount: Int {
        return pageNum * perPage
    }
    
    public init(pageNum: Int, perPage: Int) {
        self.pageNum = pageNum
        self.perPage = perPage
    }
    
    public mutating func increasePage() {
        pageNum += 1
    }
 }
