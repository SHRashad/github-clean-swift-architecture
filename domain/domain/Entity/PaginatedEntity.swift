//
//  PaginatedEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

public struct PaginatedEntity<T> {
    public var totalCount: Int
    public var items: [T]
    
    public init(totalCount: Int, items: [T]) {
        self.totalCount = totalCount
        self.items = items
    }
}
