//
//  RepositoryEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation

public struct RepositoryEntity {
    public var id: Int?
    public var name: String?
    public var fullName: String?
    public var owner: RepositoryOwnerEntity?
    public var description: String?
    public var language: String?
    public var forksCount: Int?
    public var openIssuesCount: Int?
    public var watchers: Int?
    public var createdAt: Date?
    public var pushedAt: Date?
    public var defaultBranch: String?
    public var visibility: String?
    public var htmlUrlStr: String?
    public var eventsUrlStr: String?
    public var downloadUrlStr: String?
    
    
    public init(id: Int?, name: String?, fullName: String?,
                owner: RepositoryOwnerEntity?, description: String?, lang: String?,
                forksCount: Int?, openIssuesCount: Int?, watchers: Int?,
                createdAt: Date?, pushedAt: Date?, defaultBranch: String?, visibility: String?,
                htmlUrlStr: String?, eventsUrlStr: String?, downloadUrlStr: String?  ) {
      
        self.id = id
        self.name = name
        self.fullName = fullName
        self.owner = owner
        self.description = description
        self.language = lang
        self.forksCount = forksCount
        self.openIssuesCount = openIssuesCount
        self.watchers = watchers
        self.createdAt = createdAt
        self.pushedAt = pushedAt
        self.defaultBranch = defaultBranch
        self.visibility = visibility
        self.htmlUrlStr = htmlUrlStr
        self.eventsUrlStr = eventsUrlStr
        self.downloadUrlStr = downloadUrlStr
        
    }
}
