//
//  FilterBodyEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

public enum RepoSortType: CaseIterable {
    case byStars
    case byForks
    case byUpdated
    case none
}

public struct FilterBodyEntity {
    public var searchKey: String?
    public var sortBy: RepoSortType
    
    public init() {
        self.searchKey = nil
        self.sortBy = .none
    }
}
