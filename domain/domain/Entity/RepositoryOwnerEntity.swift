//
//  RepositoryOwnerEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation

public struct RepositoryOwnerEntity {
    public var id: Int?
    public var nodeId: String?
    public var login: String?
    public var type: String?
    public var avatarUrlStr: String?
    public var urlStr: String?
    public var followersUrlStr: String?
    public var reposUrlStr: String?

    
    public init(id: Int?, nodeId: String?, login: String?,
                type: String?,avatarUrlStr: String?, urlStr: String?,
                followersUrlStr: String?, reposUrlStr: String?) {
        self.id = id
        self.nodeId = nodeId
        self.login = login
        self.type = type
        self.avatarUrlStr = avatarUrlStr
        self.urlStr = urlStr
        self.followersUrlStr = followersUrlStr
        self.avatarUrlStr = avatarUrlStr
        self.reposUrlStr = reposUrlStr
    }
}
