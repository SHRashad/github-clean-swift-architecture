//
//  ProjectsRepositoryProtocol.swift
//  domain
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import RxSwift
//i put name projects , because if i named it repository, it will be difficult to read.

public protocol ProjectsRepositoryProtocol {
    
    func getRepositories(paging: PagingEntity, body: FilterBodyEntity) -> Observable<PaginatedEntity<RepositoryEntity>>
}
