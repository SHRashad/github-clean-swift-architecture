//
//  DomainAssemblies.swift
//  domain
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Swinject

public class DomainAseemblies{
    
    public static var assemblies: [Assembly]{
        return [
            UseCasesDI()
        ]
    }
    
}
