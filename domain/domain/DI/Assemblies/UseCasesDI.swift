//
//  UseCasesDI.swift
//  domain
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Swinject

public class UseCasesDI : Assembly{
    public init() {
    }
    
    
    public func assemble(container: Container) {
        
        container.register(GetRepositoryListUseCase.self) { r in
            let repo = r.resolve(ProjectsRepositoryProtocol.self)!
            return GetRepositoryListUseCase(repo: repo)
        }
    }
}
