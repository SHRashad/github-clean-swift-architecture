//
//  BaseUseCase.swift
//  domain
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation

protocol BaseUseCase {
    associatedtype InputType
    
    associatedtype OutputType
    
    func execute(input: InputType) -> OutputType
}
