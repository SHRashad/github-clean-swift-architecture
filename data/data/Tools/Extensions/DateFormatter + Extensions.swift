//
//  DateFormatter + Extensions.swift
//  data
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

extension DateFormatter {
    static var apiDateAndTime:DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }
}
