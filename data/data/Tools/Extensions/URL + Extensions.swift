//
//  URL + Extensions.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation

extension URL {
    
    mutating func setQueryItems ( queryItems: [URLQueryItem] ) {
        guard var urlComponents = URLComponents(string: self.absoluteString) else{return}
        urlComponents.queryItems = queryItems
        urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        guard let newUrl = urlComponents.url else{return }
        self = newUrl
    }
}
