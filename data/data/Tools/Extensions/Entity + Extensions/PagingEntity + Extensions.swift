//
//  PagingEntity + Extensions.swift
//  data
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import domain

extension PagingEntity : EntityToDtoMapper  {
    typealias output = PagingDto
    
    func map() -> PagingDto {
        return PagingDto(page: pageNum, perPage: perPage)
    }
}
