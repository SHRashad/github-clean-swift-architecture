//
//  FilterBodyEntity + Extensions.swift
//  data
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import domain

extension FilterBodyEntity : EntityToDtoMapper {
    typealias dto = FilterBodyDto
    
    func map() -> dto {
        var sort: String?
        var searchKey: String
        
        switch self.sortBy {
        case .byStars:
            sort = "stars"
        case .byForks:
            sort = "forks"
        case .byUpdated:
            sort = "help-wanted-issues"
        case .none:
            sort = nil
        }
        
        if let key = self.searchKey, !key.isEmpty {
            searchKey = key
        }
        else {
            searchKey = "a"
        }
        
        return FilterBodyDto(searchKey: searchKey, sort: sort) //searchkey is required in this endpoint, thats why i put a as a default value
    }
}
