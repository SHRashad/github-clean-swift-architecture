//
//  Interceptor.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
protocol Interceptor {
    func mutateRequest(_ request: URLRequest) -> URLRequest
}
