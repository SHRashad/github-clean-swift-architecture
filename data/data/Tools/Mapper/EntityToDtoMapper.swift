//
//  EntityToDtoMapper.swift
//  data
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

protocol EntityToDtoMapper {
    associatedtype dto
    
    func map() -> dto
}
