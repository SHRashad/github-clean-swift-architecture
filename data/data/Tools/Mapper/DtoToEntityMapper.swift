//
//  DtoToEntityMapper.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation

protocol DtoToEntityMapper {
    associatedtype entity
    
    func map() -> entity
}
