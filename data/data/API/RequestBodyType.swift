//
//  RequestBodyType.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation

enum RequestBodyType {
    case json
    case multipart(boundary: String)
    
    var header: [String:String] {
        switch self {
        case .json:
            return ["Content-Type":"application/json"]
        case .multipart(let boundary):

            return ["Content-Type":"multipart/form-data; boundary=\(boundary)"]
        }
    }
}
