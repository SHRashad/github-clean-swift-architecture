//
//  ApiProtocol.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation

protocol ApiProtocol {
    var urlStr: String { get }
//    var baseType: ApiBaseType {get} // content, bla blac
    var url: URL {get}
    var version: Int? { get } 
}
