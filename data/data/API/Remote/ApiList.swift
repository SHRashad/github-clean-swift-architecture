//
//  ApiList.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation

enum RemoteAPI : ApiProtocol {
    static let baseUrl = "https://api.github.com/"
    
    
    case getRepositoryList(queryParams: [URLQueryItem])
    
    
    var urlStr: String {
        switch self {
        case .getRepositoryList:
            return "search/repositories"
        }
    }
    
    var url: URL {
        let str: String
        if version == nil {
            str = String(format: "%@%@", RemoteAPI.baseUrl, urlStr)
        }
        else {
            str = String(format: "%@v%d%@", RemoteAPI.baseUrl, version! , urlStr)
        }
        
        var url = URL(string: str)!
        
        switch self {
        case .getRepositoryList(queryParams: let queryItems):
            url.setQueryItems(queryItems: queryItems)
            return url
        default:
            return url
        }
        
    }
    
    var version: Int? {
        switch self {
            
        default:
            return nil
        }
    }
}
