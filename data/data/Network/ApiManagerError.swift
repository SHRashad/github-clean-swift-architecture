//
//  ApiManagerError.swift
//  data
//
//  Created by Rashad Shirizada on 04.11.21.
//

import Foundation

enum ApiManagerError : Error {
    case notHttpUrlResponse
    case unauthorized
    case notFound
    case badRequest
    case unhandledError(statusCode: Int)
    case custom(message: String)
}

extension ApiManagerError: LocalizedError {
    
    var errorDescription: String?{
        
        switch self {
        case .notHttpUrlResponse:
            return "Incorrect http url response"
        case .unauthorized:
            return "Unauthorized"
        case .notFound:
            return "Not found"
        case .badRequest:
            return "Bad request"
        case .unhandledError(let statusCode):
            return "Unhandled error - \(statusCode)"
        case .custom(let message):
            return message
        }
    }
}
