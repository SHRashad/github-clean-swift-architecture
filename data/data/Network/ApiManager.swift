//
//  ApiManager.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation
import RxSwift

class ApiManager {
    private let interceptor: Interceptor
    
    static let shared = ApiManager(interceptor: RequestInterceptor())
    
    private init(interceptor: Interceptor) {
        self.interceptor = interceptor
    }
    
    func fetch<T: Decodable> (apiRequest: ApiRequestProtocol) -> Single<T> {
        
        return Single.create { single in
            let request = apiRequest.request
            let session = URLSession.shared
            let mutatedRequest = self.interceptor.mutateRequest(request)
            debugPrint(request)
            session.dataTask(with: mutatedRequest) { (data, response, error) in
                if let error = error{
                    single(.failure(error))
                }
                else{
                    ApiResponseHandler.shared.handle(single: single, api: apiRequest, response: response, data: data)
                    
                }
            }.resume()
            
            return Disposables.create()
        }
    }

}
