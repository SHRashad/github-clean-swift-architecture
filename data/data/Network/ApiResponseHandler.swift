//
//  ApiResponseHandler.swift
//  data
//
//  Created by Rashad Shirizada on 04.11.21.
//

import Foundation
import RxSwift
class ApiResponseHandler {
    static let shared = ApiResponseHandler()
    private init() {
        
    }
    
    func handle<T: Decodable> (single: ((SingleEvent<T>) -> Void),
                               api: ApiRequestProtocol,
                               response: URLResponse?,
                               data: Data?) {
        guard let response = response as? HTTPURLResponse else{
            single(.failure(ApiManagerError.notHttpUrlResponse))
            return
        }
        let statusCode = response.statusCode
//        debugPrint(api,statusCode,response)
        switch statusCode {
        case 200,201:
            guard let data = data else {
                return
            }
            print("JSON",try? JSONSerialization.jsonObject(with: data, options: []))
            do {
                let model = try JSONDecoder().decode(T.self, from: data)
                single(.success(model))
            }
            catch {
                single(.failure(error))
            }
        case 400, 404:
            print("400", " 404 case" )
//            guard let data = data else {
//                single(.failure(ApiManagerError.unhandledError(statusCode: statusCode)))
//                return
//            }
//            let errorResponse = try? JSONDecoder().decode(ErrorResponseDto.self, from: data)
//            single(.failure(ApiManagerError.custom(message: errorResponse?.message ?? "")))
//
        case 401:
            single(.failure(ApiManagerError.unauthorized))
        case 422:
            print("validation error")
//            guard let data = data else {
//                single(.failure(ApiManagerError.unhandledError(statusCode: statusCode)))
//                return
//            }
//            let errorResponse = try? JSONDecoder().decode(ValidationErrorReponseDto.self, from: data)
//            single(.failure(ApiManagerError.custom(message: errorResponse?.fullMessage ?? "")))
        default:
            single(.failure(ApiManagerError.unhandledError(statusCode: statusCode)))
        }
    }
}
