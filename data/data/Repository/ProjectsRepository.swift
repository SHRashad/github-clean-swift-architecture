//
//  ProjectsRepository.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import domain
import RxSwift

class ProjectsRepository : ProjectsRepositoryProtocol {
    
    
    let remoteDataSource: RepositoryRemoteDataSourceProtocol
    
    init(remoteDataSource: RepositoryRemoteDataSourceProtocol) {
        self.remoteDataSource = remoteDataSource
    }
    
    func getRepositories(paging: PagingEntity, body: FilterBodyEntity) -> Observable<PaginatedEntity<RepositoryEntity>>  {
        return remoteDataSource.fetchRepositories(paging: paging.map(), body: body.map())
            .map({$0.map()}).asObservable()

    }
    
}
