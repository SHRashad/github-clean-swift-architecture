//
//  DataAssemblies.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Swinject

public class DataAssemblies{
    public static var assemblies: [Assembly]{
        return [
            RepositoryDI(),
            RemoteDataSourceDI(),
            HelpersDI()
        ]
    }

}
