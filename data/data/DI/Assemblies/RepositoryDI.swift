//
//  RepositoryDI.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Foundation
import Foundation
import Swinject
import domain

class RepositoryDI : Assembly{
    
    public init() {
    }
    
    public func assemble(container: Container) {
        container.register(ProjectsRepositoryProtocol.self) { r in
            let remoteDataSource = r.resolve(RepositoryRemoteDataSourceProtocol.self)!
            return ProjectsRepository(remoteDataSource: remoteDataSource)
        }
    }
    
}
