//
//  HelpersDI.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Foundation
import Foundation
import Swinject
import domain

class HelpersDI : Assembly{
    
    public init() {
    }
    
    public func assemble(container: Container) {
        container.register(ApiManager.self) { r in
            return .shared
        }
    }
    
}
