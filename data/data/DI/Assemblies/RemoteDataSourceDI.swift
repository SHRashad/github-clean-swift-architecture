//
//  RemoteDataSourceDI.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Foundation
import Swinject
import domain

class RemoteDataSourceDI : Assembly{
    
    public init() {
    }
    
    public func assemble(container: Container) {
        
        container.register(RepositoryRemoteDataSourceProtocol.self) { r in
            let apiManager = r.resolve(ApiManager.self)!
            return RepositoryRemoteApiDataSource(apiManager: apiManager)
        }
    }
    
}
