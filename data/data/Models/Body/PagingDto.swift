//
//  PagingDto.swift
//  data
//
//  Created by Rashad Shirizada on 26.01.22.
//
import Foundation
import domain

struct PagingDto  {
    var page: Int
    var perPage: Int
}

extension PagingDto {
    var queryItems: [URLQueryItem]  {
        return [
            URLQueryItem(name: "page", value: String(page)),
            URLQueryItem(name: "per_page", value: String(perPage))
        ]
    }
}
