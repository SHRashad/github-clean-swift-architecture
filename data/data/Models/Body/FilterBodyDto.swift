//
//  FilterBodyDto.swift
//  data
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

struct FilterBodyDto {
    var searchKey: String
    var sort: String?
    
    init(searchKey: String, sort: String?) {
        self.searchKey = searchKey
        self.sort = sort
    }
}

extension FilterBodyDto  {
    var queryItems: [URLQueryItem]  {
        var items : [URLQueryItem] = []
        items.append(URLQueryItem(name: "q", value: self.searchKey))
        if let sort = sort {
            items.append(URLQueryItem(name: "sort", value: sort))
        }
        return items
    }
}
