//
//  PaingatedDto.swift
//  data
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import domain

struct PaginatedDto < T : Decodable> : Decodable where T: DtoToEntityMapper {
    var totalCount: Int
    var incompleteResults: Bool
    var items: [T]
    
    enum CodingKeys : String, CodingKey{
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case items = "items"
    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.totalCount = try container.decode(Int.self, forKey: .totalCount)
        self.incompleteResults = try container.decodeIfPresent(Bool.self, forKey: .incompleteResults) ?? false
        self.items = try container.decode([T].self, forKey: .items)
    }
}

extension PaginatedDto : DtoToEntityMapper {
    typealias output = PaginatedEntity<T.entity>
    
    
    func map() -> output {
        return PaginatedEntity(totalCount: totalCount, items: items.map({$0.map()}))
    }
}
