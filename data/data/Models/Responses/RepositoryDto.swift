//
//  RepositoryDto.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import domain

struct RepositoryDto: Decodable {
    var id: Int?
    var name: String?
    var fullName: String?
    var owner: RepositoryOwnerDto?
    var description: String?
    var language: String?
    var forksCount: Int?
    var openIssuesCount: Int?
    var watchers: Int?
    var createdAt: String?
    var pushedAt: String?
    var defaultBranch: String?
    var visibility: String?
    var htmlUrlStr: String?
    var eventsUrlStr: String?
    var downloadUrlStr: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case owner
        case description
        case language
        case forksCount = "forks_count"
        case openIssuesCount = "open_issues_count"
        case watchers
        case createdAt  = "created_at"
        case pushedAt = "pushed_at"
        case defaultBranch = "default_branch"
        case visibility
        case htmlUrl = "html_url"
        case eventUrl = "events_url"
        case downloadUrl = "downloads_url"
    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.fullName = try container.decodeIfPresent(String.self, forKey: .fullName)
        self.owner = try container.decodeIfPresent(RepositoryOwnerDto.self, forKey: .owner)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.language = try container.decodeIfPresent(String.self, forKey: .language)
        self.forksCount = try container.decodeIfPresent(Int.self, forKey: .forksCount)
        self.openIssuesCount = try container.decodeIfPresent(Int.self, forKey: .openIssuesCount)
        self.watchers = try container.decodeIfPresent(Int.self, forKey: .watchers)
        self.createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt)
        self.pushedAt = try container.decodeIfPresent(String.self, forKey: .pushedAt)
        self.defaultBranch = try container.decodeIfPresent(String.self, forKey: .defaultBranch)
        self.visibility = try container.decodeIfPresent(String.self, forKey: .visibility)
        self.htmlUrlStr = try container.decodeIfPresent(String.self, forKey: .htmlUrl)
        self.eventsUrlStr = try container.decodeIfPresent(String.self, forKey: .eventUrl)
        self.downloadUrlStr = try container.decodeIfPresent(String.self, forKey: .downloadUrl)
    }
}


extension RepositoryDto: DtoToEntityMapper {
    typealias entity = RepositoryEntity
    
    func formatDate(dateStr: String?, formatter: DateFormatter) -> Date? {
        if let dateStr = dateStr {
            return formatter.date(from: dateStr)
        }
        return nil
    }
    
    func map() -> entity {
        return RepositoryEntity(id: id,
                                name: name,
                                fullName: fullName,
                                owner: owner?.map(),
                                description: description,
                                lang: language,
                                forksCount: forksCount,
                                openIssuesCount: openIssuesCount,
                                watchers: watchers,
                                createdAt: formatDate(dateStr: createdAt, formatter: .apiDateAndTime),
                                pushedAt: formatDate(dateStr: pushedAt, formatter: .apiDateAndTime),
                                defaultBranch: defaultBranch,
                                visibility: visibility,
                                htmlUrlStr: htmlUrlStr,
                                eventsUrlStr: eventsUrlStr,
                                downloadUrlStr: downloadUrlStr
        )
    }
}
