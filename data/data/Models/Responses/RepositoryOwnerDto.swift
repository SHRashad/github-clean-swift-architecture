//
//  RepositoryOwnerDto.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import domain


struct RepositoryOwnerDto: Decodable {
    var id: Int?
    var nodeId: String?
    var login: String?
    var type: String?
    var avatarUrlStr: String?
    var urlStr: String?
    var followersUrlStr: String?
    var reposUrlStr: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case nodeId = "node_id"
        case login
        case type
        case avatarUrl = "avatar_url"
        case followersUrl = "followers_url"
        case reposUrl = "repos_url"
        case url
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.nodeId = try container.decodeIfPresent(String.self, forKey: .nodeId)
        self.login = try container.decodeIfPresent(String.self, forKey: .login)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
        self.avatarUrlStr = try container.decodeIfPresent(String.self, forKey: .avatarUrl)
        self.urlStr = try container.decodeIfPresent(String.self, forKey: .url)
        self.followersUrlStr = try container.decodeIfPresent(String.self, forKey: .followersUrl)
        self.reposUrlStr = try container.decodeIfPresent(String.self, forKey: .reposUrl)
    }
}

extension RepositoryOwnerDto: DtoToEntityMapper {
    typealias entity = RepositoryOwnerEntity
    
    
    func map() -> RepositoryOwnerEntity {
       return RepositoryOwnerEntity(id: id, nodeId: nodeId, login: login, type: type, avatarUrlStr: avatarUrlStr, urlStr: urlStr, followersUrlStr: followersUrlStr, reposUrlStr: reposUrlStr)
    }
    
    
}
