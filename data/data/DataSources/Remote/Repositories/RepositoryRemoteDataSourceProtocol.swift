//
//  RepositoryRemoteDataSourceProtocol.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import RxSwift

protocol RepositoryRemoteDataSourceProtocol {
    func fetchRepositories(paging: PagingDto, body: FilterBodyDto) -> Single<PaginatedDto<RepositoryDto>>
}
