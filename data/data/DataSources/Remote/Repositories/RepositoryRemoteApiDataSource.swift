//
//  RepositoryRemoteApiDataSource.swift
//  data
//
//  Created by Rashad Shirizada on 25.01.22.
//

import RxSwift

class RepositoryRemoteApiDataSource: RepositoryRemoteDataSourceProtocol {
    
    let apiManager: ApiManager
    
    init(apiManager: ApiManager) {
        self.apiManager = apiManager
    }
    
    func fetchRepositories(paging: PagingDto, body: FilterBodyDto) -> Single<PaginatedDto<RepositoryDto>> {
        var queryItems: [URLQueryItem] = paging.queryItems
        queryItems.append(contentsOf: body.queryItems)
        
        let api : RemoteAPI = .getRepositoryList(queryParams: queryItems)
        let apiRequest = ApiRequest(api: api, method: .get)
        
        return apiManager.fetch(apiRequest: apiRequest)
    }
}
