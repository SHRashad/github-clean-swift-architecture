//
//  FilterButton.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit

class FilterButton: BaseButton {

 
    override func setupView() {
        super.setupView()
        
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
    }
    
}
