//
//  LinkButton.swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import UIKit

class LinkButton: BaseButton {


    override func setupView() {
        super.setupView()
        
        self.layer.cornerRadius = 10
        self.backgroundColor = UIColor.blue
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel!.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        self.titleLabel!.textAlignment = .center
    }

}
