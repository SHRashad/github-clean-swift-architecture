//
//  PopupBackgroundView.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit

class PopupBackgroundView: UIView {

    override func layoutSubviews() {
        self.roundCorners(corners: [.topLeft,.topRight], radius: 14)
        self.backgroundColor = .white
    }
}
