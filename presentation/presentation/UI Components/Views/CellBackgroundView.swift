//
//  CellBackgroundView.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit

class CellBackgroundView: BaseView {

    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.applyBlackShadow()
    }
    
    override func setupView() {
        super.setupView()
        self.backgroundColor = .white
        self.layer.cornerRadius = 10
    }
}
