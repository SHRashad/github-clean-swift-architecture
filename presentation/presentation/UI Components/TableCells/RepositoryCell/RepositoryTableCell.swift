//
//  RepositoryTableCell.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit
import domain
import RxSwift

enum RepositoryCellEvent {
    case userDetailTapped(repo: RepositoryEntity)
}

class RepositoryTableCell: UITableViewCell, RegisterableCell {
    static var reuseIdentifier: String{
        return "repository table cell"
    }
    
    static var nibName: String {
        return "RepositoryTableCell"
    }
    

    @IBOutlet weak var authorPp: UIImageView!
    @IBOutlet weak var repoNameLbl: UILabel!
    @IBOutlet weak var authorNameLbl: UILabel!
    @IBOutlet weak var watchersNumLbl: UILabel!
    @IBOutlet weak var forksNumLbl: UILabel!
    @IBOutlet weak var issuesNumLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.authorPp.layer.cornerRadius = self.authorPp.frame.height / 2
    }

    func setup(repository: RepositoryEntity, event: PublishSubject<RepositoryCellEvent>) {
        self.repoNameLbl.text = repository.fullName
        self.watchersNumLbl.text = "\(repository.watchers ?? 0)"
        self.forksNumLbl.text = "\(repository.forksCount ?? 0)"
        self.issuesNumLbl.text = "\(repository.openIssuesCount ?? 0)"
        self.dateLbl.text = repository.formattedCreatedAt
        
        setOwnerDetails(owner: repository.owner)
        self.handleEvents(event: event, repo: repository)
    }
    
    private func setOwnerDetails(owner: RepositoryOwnerEntity?) {
        self.authorPp.loadImage(assetType: owner?.pp, option: .circle)
        self.authorNameLbl.text = owner?.login
    }
    
    private func handleEvents(event: PublishSubject<RepositoryCellEvent>, repo: RepositoryEntity) {
        self.authorPp.addTapGesture {
            event.onNext(.userDetailTapped(repo: repo))
        }
    }
}
