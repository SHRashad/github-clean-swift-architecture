//
//  CustomSearchbar.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import UIKit


class CustomSearchbar: UISearchBar {
    var bgColor: UIColor?

    override func layoutSubviews() {
        super.layoutSubviews()
        updateTextfieldFrame()
        self.layer.cornerRadius = 10
    }
    
    init(bgColor: UIColor? = .clear){
        self.bgColor = bgColor
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup(){
        self.clipsToBounds = true
        self.isTranslucent = false
        self.backgroundImage = UIImage()
        self.setBackgroundImage(UIImage(), for: .top, barMetrics: .default)
        
        self.layer.borderWidth = 2
        self.backgroundColor = bgColor
        
        updateTextfieldView()
    }
    
    private func updateTextfieldFrame(){
        for myView in self.subviews  {
            for mySubView in myView.subviews  {
                for mySubSubView in mySubView.subviews{
                    if let textField = mySubSubView as? UITextField {
                        textField.backgroundColor = bgColor
                        textField.textColor = .black
                    }
                }
            }
        }
    }
    
    private func updateTextfieldView(){
        for myView in self.subviews  {
            for mySubView in myView.subviews  {
                for mySubSubView in mySubView.subviews{
                    if let textField = mySubSubView as? UITextField {
                        var bounds = textField.frame
                        bounds.size.height = 48
                        textField.bounds = bounds
                        textField.layer.cornerRadius = 10
                    }
                }
            }
        }
    }
}
