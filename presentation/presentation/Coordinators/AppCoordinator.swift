//
//  AppCoordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import UIKit

public class AppCoordinator {
    
    
    let window: UIWindow
    let provider: ProviderProtocol
    
    var repositoryCoordinator: RepositoryCoordinator?
    
     init(window: UIWindow,
                provider: ProviderProtocol) {
        self.window = window
        self.provider = provider
    }
    
    public func start() {

        startRepositoryFlow()
    }
    
    func startRepositoryFlow () {
        
        let navigation = UINavigationController()
        repositoryCoordinator = self.provider.repositoryCoordinator(navigation: navigation)
        self.repositoryCoordinator?.start()
        
        self.window.rootViewController = navigation
    }
}
