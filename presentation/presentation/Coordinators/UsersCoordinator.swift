//
//  UsersCoordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import Foundation
import UIKit
import RxSwift
import domain

class UsersCoordinator: BaseCoordiantor {

    
    func showUserDetail(owner: RepositoryOwnerEntity) {
        let ownerController = self.controllerProvider.repoOwnerController(owner: owner)
        self.navigationController.pushViewController(ownerController, animated: true)
    }
    
    
}
