//
//  RepositoryCoordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import UIKit
import RxSwift
import domain

class RepositoryCoordinator: BaseCoordiantor {
    var disposeBag = DisposeBag()
    
    var usersCoordinator: UsersCoordinator?
    
    override init(navigationController: UINavigationController, controllerProvider: ProviderProtocol) {
        super.init(navigationController: navigationController, controllerProvider: controllerProvider)
        usersCoordinator = controllerProvider.usersCoordinator(navigation: navigationController)
    }
    
    override func start() {
        
        let repositoryListController = controllerProvider.repositoryListController
        self.navigationController.pushViewController(repositoryListController, animated: true)
        repositoryListController.title = "Repository List"
        
        repositoryListController.onItem = { indexPath in
            guard let repositoryItem = repositoryListController.repositoryListViewModel.getItem(at: indexPath.row)
            else { return }
            self.openRepositoryDetails(repositoryEntity: repositoryItem)
        }
        
        repositoryListController.onFilter = {
            let sortItems = repositoryListController.repositoryListViewModel.getSortItems()
            
            self.openSort(items: sortItems)
                .do(onNext: { filterBody in
                    if let selectedFilter = filterBody.first(where: {$0.state})?.item {
                        repositoryListController.repositoryListViewModel.filterBody.sortBy = selectedFilter
                    }
                    else {
                        repositoryListController.repositoryListViewModel.filterBody.sortBy = .none
                    }
                }).subscribe().disposed(by: self.disposeBag)
        }
        
        repositoryListController.repoCellEvent.do(onNext: {event in
            switch event {
            case .userDetailTapped(repo: let repo):
                guard let owner = repo.owner else { return }
                self.usersCoordinator?.showUserDetail(owner: owner)
            }
        }).subscribe().disposed(by: self.disposeBag)
            
            }
    
    
    func openSort<T:SortItemProtocol>(items: [SortItem<T>]) -> PublishSubject<[SortItem<T>]>  {
        let sortController = SortController<T>(title: "Sort by type", items: items)
        self.navigationController.presentFully(sortController)
        return sortController.onFilter
    }
    
    func openRepositoryDetails(repositoryEntity: RepositoryEntity) {
        let repoDetailsController = controllerProvider.repositoryDetailsController(repo: repositoryEntity)
        self.navigationController.pushViewController(repoDetailsController, animated: true)
    }
}
