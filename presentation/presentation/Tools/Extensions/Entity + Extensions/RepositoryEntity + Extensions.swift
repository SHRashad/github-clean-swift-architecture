//
//  RepositoryEntity + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import domain

extension RepositoryEntity {
    var formattedCreatedAt: String? {
        if let date = createdAt {
            return DateFormatter.repositoryDate.string(from: date)
        }
        return nil
    }
    
    var formattedPushdeAt: String? {
        if let date = pushedAt {
            return DateFormatter.repositoryDate.string(from: date)
        }
        return nil
    }
}
