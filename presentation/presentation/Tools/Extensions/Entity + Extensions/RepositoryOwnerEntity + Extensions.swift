//
//  RepositoryOwnerEntity + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import domain

extension RepositoryOwnerEntity {
    var pp: AssetType {
        if let avatarUrlStr = avatarUrlStr, let url = avatarUrlStr.url {
            return .remote(url: url)
        }
        else {
            return .localByName(name: "some pp placeholder")
        }
    }
}
