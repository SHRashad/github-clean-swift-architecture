//
//  RepoSortType + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import domain

extension RepoSortType : SortItemProtocol {
    var name: String? {
        switch self {
        case .byStars:
            return "Sort by stars"
        case .byForks:
            return "Sort by forks"
        case .byUpdated:
            return "Sort by updated"
        case .none:
            return "None"
        }
    }
}
