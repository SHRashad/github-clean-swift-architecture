//
//  UIScroll + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import UIKit
import RxSwift

extension UIScrollView {
    func enablePagination (viewmodel: PaginatableViewModel) -> Disposable {
        var mutatingVm = viewmodel
        return  self.rx.didScroll.subscribe(onNext: { _ in
            if (((self.contentOffset.y + self.frame.size.height) >= self.contentSize.height )){
               
                if viewmodel.hasNext && !viewmodel.isCurrentlyFetchingData {
                    mutatingVm.paging.increasePage()
                    mutatingVm.loadMore()
                }
            
            }
        })
    }
}
