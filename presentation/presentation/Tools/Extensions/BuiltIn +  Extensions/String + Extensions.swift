//
//  String + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

extension String  {
    var url: URL?  {
        return URL ( string: self)
    }
}
