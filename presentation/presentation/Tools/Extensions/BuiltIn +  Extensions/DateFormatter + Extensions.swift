//
//  DateFormatter + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//
import Foundation

extension DateFormatter {
    static var repositoryDate:DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        return formatter
    }
}
