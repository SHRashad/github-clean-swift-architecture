//
//  CALayer + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit

extension CALayer {

    func applyBlackShadow() {
        self.shadowOffset = CGSize(width: 0, height: 1)
        self.shadowColor = UIColor.black.cgColor
        self.shadowRadius = 4
        self.shadowOpacity = 0.1
    }
}
