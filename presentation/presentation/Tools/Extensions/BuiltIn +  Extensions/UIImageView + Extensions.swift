//
//  UIImageView + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import Foundation
import UIKit
import RxSwift
import RxCocoa
import Kingfisher

enum UIImageOptions {
    case circle
    case rounded(radius: CGFloat)
}

extension UIImageView{
    
    func loadImage(assetType: AssetType?, option: UIImageOptions?  = nil){
        guard let assetType = assetType else {
            return
        }

        switch assetType {
        case .localByName(let imageName):
            DispatchQueue.main.async {
                self.image = UIImage(named: imageName)
            }
        case .localUI(image: let image):
            DispatchQueue.main.async {
                self.image = image
            }
        case .remote(let url):
            var kingfisherOption: KingfisherOptionsInfo = [.transition(.flipFromRight(0.4))]
            switch option {
            case .circle:
                kingfisherOption.insert(contentsOf: self.roundOptions, at: kingfisherOption.count)
            case .rounded(radius: let radius):
                kingfisherOption.insert( contentsOf: self.roundOptions(round: radius), at: kingfisherOption.count)
            default:
                print("")
            }
            self.kf.setImage(with: url, options: kingfisherOption)
        }
    }
    
    var roundProcessor:RoundCornerImageProcessor{
        return RoundCornerImageProcessor(cornerRadius: self.frame.height / 2)
    }
    var roundOptions:KingfisherOptionsInfo{
        return [.processor(roundProcessor)]
    }

    func roundOptions(round:CGFloat) ->KingfisherOptionsInfo{
        let roundProcessor =  RoundCornerImageProcessor(cornerRadius: round)
        return [.processor(roundProcessor)]

    }
}
