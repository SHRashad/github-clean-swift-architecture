//
//  UIView + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import UIKit

extension UIView  {
    
    func fixView( _ contentView: UIView, padding: UIEdgeInsets = .zero) {
        self.addSubview(contentView)
        contentView.snp.makeConstraints { maker in
            maker.top.equalToSuperview().offset(padding.top)
            maker.leading.equalToSuperview().offset(padding.left)
            maker.trailing.equalToSuperview().offset(-padding.right)
            maker.bottom.equalToSuperview().offset(-padding.bottom)
        }
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func addTapGesture(action: (() -> Void)? = nil) {
        let gesture = MyTapGestureRecognizer(target: self, action: #selector(handleTap))
        gesture.action = action
        gesture.cancelsTouchesInView = true
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(gesture)
    }
    
    @objc func handleTap(sender:MyTapGestureRecognizer) {
        sender.action?()
    }
    
}

class MyTapGestureRecognizer: UITapGestureRecognizer {
    var action : (() -> Void)?
}
