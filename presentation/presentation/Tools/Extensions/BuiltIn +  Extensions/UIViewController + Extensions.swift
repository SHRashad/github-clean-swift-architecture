//
//  UIViewController + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func presentFully(_ controller: UIViewController, _ animated: Bool = true) {
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: animated, completion: nil)
    }
}
