//
//  DeeplinkManager.swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import Foundation
import UIKit
import SafariServices

class DeeplinkManager {
    static let shared = DeeplinkManager()
    
    private init() {
        
    }
    
    func openInSafari(url: URL, from: UIViewController) {
        let safariController = SFSafariViewController(url: url)
        from.present(safariController, animated: true, completion: nil)
    }
    
    func openUrl(url: URL) {
        
    }
}
