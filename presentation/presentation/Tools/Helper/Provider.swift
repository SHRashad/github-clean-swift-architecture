//
//  Provider.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Swinject
import UIKit
import domain

protocol ProviderProtocol {
    func appCoordinator(window: UIWindow) -> AppCoordinator
    func repositoryCoordinator(navigation: UINavigationController) -> RepositoryCoordinator
    func usersCoordinator(navigation: UINavigationController) -> UsersCoordinator

    var repositoryListController: RepositoryListController { get }
    func repositoryDetailsController(repo: RepositoryEntity) -> RepositoryDetailsController
    func repoOwnerController(owner: RepositoryOwnerEntity) -> RepositoryOwnerController

}

class ControllerProviders: ProviderProtocol {
  
    let resolver: Resolver
    public init(resolver: Resolver){
        self.resolver = resolver
    }
    
    func appCoordinator(window: UIWindow) -> AppCoordinator {
        return resolver.resolve(AppCoordinator.self, argument: window)!
    }
    
    func repositoryCoordinator(navigation: UINavigationController) -> RepositoryCoordinator {
        return resolver.resolve(RepositoryCoordinator.self, argument: navigation)!
    }
    
    func usersCoordinator(navigation: UINavigationController) -> UsersCoordinator {
        return resolver.resolve(UsersCoordinator.self, argument: navigation)!
    }
    
    
    //controllers
    var repositoryListController: RepositoryListController {
        let vm = resolver.resolve(RepositoryListViewModel.self)!
        return RepositoryListController(repositoryListViewModel: vm)
    }
    
    func repositoryDetailsController(repo: RepositoryEntity) -> RepositoryDetailsController {
        let vm = resolver.resolve(RepositoryDetailViewModel.self, argument: repo)!
        return RepositoryDetailsController(repositoryDetailViewModel: vm)
    }
    
    func repoOwnerController(owner: RepositoryOwnerEntity) -> RepositoryOwnerController {
        let vm = resolver.resolve(RepositoryOwnerViewModel.self, argument: owner)!
        return RepositoryOwnerController(ownerViewModel: vm)
    }
}
