//
//  PresentationAssemblies.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//
import Foundation
import Swinject
import UIKit

public class PresentationAssemblies{
    
    public static var assemblies: [Assembly]{
        return [
            CoordinatorDI(),
            ControllerDI(),
            ViewModelDI()
        ]
    }
    
}
