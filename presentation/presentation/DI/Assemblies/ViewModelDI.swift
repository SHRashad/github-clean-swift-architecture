//
//  ViewModelDI.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Swinject
import domain

public class ViewModelDI : Assembly{
    public init() {
    }
    
    public func assemble(container: Container) {
        
        container.register(RepositoryListViewModel.self) { r in
            let getRepositoryListUseCase = r.resolve(GetRepositoryListUseCase.self)!
            
            return RepositoryListViewModel(getRepositoryListUseCase: getRepositoryListUseCase)
        }
        
        container.register(RepositoryDetailViewModel.self) { (r, repo: RepositoryEntity) in
            let repoDetailViewModel = RepositoryDetailViewModel(repository: repo)
            return repoDetailViewModel
        }
        
        container.register(RepositoryOwnerViewModel.self) { (r, owner: RepositoryOwnerEntity) in
            let ownerVM = RepositoryOwnerViewModel(owner: owner)
            return ownerVM
        }
    }
}
