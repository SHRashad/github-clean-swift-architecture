//
//  ControllerDI.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import Foundation
import Swinject
import UIKit

public class ControllerDI : Assembly{
    public init() {
    }
    
    public func assemble(container: Container) {
        container.register(ControllerProviders.self) { r in
            return ControllerProviders(resolver: r)
        }
    }
}
