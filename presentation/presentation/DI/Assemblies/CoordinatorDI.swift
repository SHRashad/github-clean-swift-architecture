//
//  CoordinatorDI.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//
import Foundation
import Swinject
import UIKit

public class CoordinatorDI : Assembly{
    public init() {
    }
    
    public func assemble(container: Container) {
      
        container.register(AppCoordinator.self) { (r,window: UIWindow) in
            let provider = r.resolve(ControllerProviders.self)!
            return AppCoordinator(window: window, provider: provider)
        }
        
        
        container.register(RepositoryCoordinator.self) { (r,nav: UINavigationController) in
            let provider = r.resolve(ControllerProviders.self)!
            return RepositoryCoordinator(navigationController: nav,
                                         controllerProvider: provider)
        }
        
        container.register(UsersCoordinator.self) { (r,nav: UINavigationController) in
            let provider = r.resolve(ControllerProviders.self)!
            return UsersCoordinator(navigationController: nav,
                                         controllerProvider: provider)
        }
    }
}
