//
//  SortItem.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation

protocol SortItemProtocol {
    var name: String? { get }
}

struct SortItem<T: SortItemProtocol> {
    var item: T
    var state: Bool
}
