//
//  AssetType.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import UIKit

enum AssetType{
    case localByName(name: String)
    case localUI(image: UIImage)
    case remote(url: URL)
}
