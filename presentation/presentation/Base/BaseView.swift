//
//  BaseView.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit

class BaseView: UIView {


    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() { }
}
