//
//  BaseScrollableController.swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import UIKit

class BaseScrollableController: BaseController {
    var scrollView: UIScrollView!
    var contentView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func setupView() {
        super.setupView()
        scrollView  = UIScrollView()
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { maker in
            maker.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            maker.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            maker.leading.trailing.equalToSuperview()
        }
        contentView = UIView()
        scrollView.fixView(contentView)
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: 0).isActive = true
        let heightConstraint = contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor, constant: 0)
            heightConstraint.isActive = true
        heightConstraint.priority = UILayoutPriority(250)

    }

}
