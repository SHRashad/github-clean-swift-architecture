//
//  BasePopupController.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit

class BasePopupController: BaseController {
    private var backgroundView: UIView!
     var containerView: UIView!


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func containerBottomSwiped() {
        dismissController()
    }

    override func setupView() {
        super.setupView()
        self.view.backgroundColor = .clear

        addBackgroundView()
        addContainerView()

    }

    private func addBackgroundView() {
        backgroundView = UIView()
        view.fixView(backgroundView)
        backgroundView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        backgroundView.addTapGesture {[weak self] in
            self?.dismissController()
        }
    }

    private func addContainerView() {

        containerView = PopupBackgroundView()
        view.addSubview(containerView)
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(containerBottomSwiped))
        swipeGesture.direction = .down
        containerView.addGestureRecognizer(swipeGesture)
        containerView.snp.makeConstraints { maker in
            maker.bottom.leading.trailing.equalToSuperview()
            maker.top.greaterThanOrEqualToSuperview()
        }
    }
}
