//
//  BaseController.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import UIKit

class BaseController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        handleEvents()
    }
    
    func handleEvents() {
        
    }
    
    func setupView() {
        self.view.backgroundColor = .white
    }
}
