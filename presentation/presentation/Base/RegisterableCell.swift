//
//  RegisterableCell.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import UIKit

protocol RegisterableCell {
    static var reuseIdentifier: String {get}
    static var nibName:String {get}
    static var nib:UINib {get}
}

extension RegisterableCell {
   static var nib: UINib {
       return UINib(nibName: nibName, bundle: Bundle.init(identifier: "com.rashad.github-assignment.presentation"))
    }
}
