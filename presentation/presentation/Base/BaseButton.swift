//
//  BaseButton.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import UIKit

class BaseButton: UIButton {
    var onTap: (()->()) = {}
    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() {
        self.addTarget(self, action: #selector(tapped), for: .touchUpInside)
    }
    
    @objc func tapped() {
        self.onTap()
    }
}
