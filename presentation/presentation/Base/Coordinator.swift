//
//  Coordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//
import UIKit

protocol Coordinator {
    var childCoordinator: [Coordinator] {get}
    var navigationController: UINavigationController {get}
    func start()
}
