//
//  BaseCoordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import UIKit

public class BaseCoordiantor: Coordinator{
    var childCoordinator: [Coordinator] = []
    
    public var navigationController: UINavigationController
    internal  var controllerProvider: ProviderProtocol
    
    init(navigationController: UINavigationController,
         controllerProvider: ProviderProtocol){
        self.navigationController = navigationController
        self.controllerProvider = controllerProvider
    }
    
    func start() {
        
    }
    
    
}
