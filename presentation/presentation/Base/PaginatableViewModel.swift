//
//  PaginatableViewModel.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import Foundation
import domain

protocol PaginatableViewModel {
    var paging: PagingEntity { set get}
    var hasNext: Bool { set get}
    var isCurrentlyFetchingData: Bool { set get}
    func loadMore()
    
    mutating func reset()
    
    func deleteStoredList()
}

extension PaginatableViewModel {
    mutating func reset() {
        self.paging.pageNum = 0
        self.hasNext = true
        self.isCurrentlyFetchingData = false
        self.deleteStoredList()
    }
    
    func deleteStoredList() {
        
    }
}
