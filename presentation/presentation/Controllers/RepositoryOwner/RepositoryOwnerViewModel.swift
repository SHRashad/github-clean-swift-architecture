//
//  OwnerViewModel.swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import Foundation
import domain
import RxSwift

class RepositoryOwnerViewModel {
    var id: PublishSubject<Int?> = PublishSubject()
    var nodeId: PublishSubject<String?> = PublishSubject()
    var assetType: PublishSubject<AssetType> = PublishSubject()
    var login: PublishSubject<String?> = PublishSubject()
    var type:  PublishSubject<String?> = PublishSubject()
    
    
    let owner: RepositoryOwnerEntity
    init(owner: RepositoryOwnerEntity) {
        self.owner = owner
    }
    
    func loadData() {
        self.id.onNext(owner.id)
        self.nodeId.onNext(owner.nodeId)
        self.login.onNext(owner.login)
        self.type.onNext(owner.type)
        self.assetType.onNext(owner.pp)
    }
}
