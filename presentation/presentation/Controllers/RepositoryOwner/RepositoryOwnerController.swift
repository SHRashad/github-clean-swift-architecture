//
//  RepositoryOwnerController.swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import UIKit
import RxSwift

class RepositoryOwnerController: BaseController {
    var disposeBag = DisposeBag()
    @IBOutlet weak var pp: UIImageView!
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var nodeIdLbl: UILabel!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var userButton: LinkButton!
    @IBOutlet weak var reposButton: LinkButton!
    @IBOutlet weak var followersButton: LinkButton!
    
    let ownerViewModel: RepositoryOwnerViewModel
    
    init(ownerViewModel: RepositoryOwnerViewModel) {
        self.ownerViewModel = ownerViewModel
        super.init(nibName: nil, bundle: Bundle(for: RepositoryDetailsController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDetails()
        ownerViewModel.loadData()
    }
    
    override func handleEvents() {
        super.handleEvents()
        
        userButton.onTap = {[weak self] in
            guard let self = self ,
                  let url = self.ownerViewModel.owner.urlStr?.url else { return }
            
            DeeplinkManager.shared.openInSafari(url: url, from: self)
        }
        
        reposButton.onTap = {[weak self] in
            guard let self = self ,
                  let url = self.ownerViewModel.owner.reposUrlStr?.url else { return }
            
            DeeplinkManager.shared.openInSafari(url: url, from: self)
        }
        
        followersButton.onTap = {[weak self] in
            guard let self = self ,
                  let url = self.ownerViewModel.owner.followersUrlStr?.url else { return }
            
            DeeplinkManager.shared.openInSafari(url: url, from: self)
        }
    }
    
    
    private func setupDetails() {
        ownerViewModel.id.compactMap({$0}).map({"\($0)"}).bind(to: idLbl.rx.text).disposed(by: self.disposeBag)
        ownerViewModel.nodeId.compactMap({$0}).bind(to: nodeIdLbl.rx.text).disposed(by: self.disposeBag)
        ownerViewModel.login.compactMap({$0}).bind(to: loginLbl.rx.text).disposed(by: self.disposeBag)
        ownerViewModel.type.compactMap({$0}).bind(to: typeLbl.rx.text).disposed(by: self.disposeBag)
        ownerViewModel.assetType.do(onNext: {asset in
            self.pp.loadImage(assetType: asset, option: .rounded(radius: 2))
        }).subscribe().disposed(by: self.disposeBag)
    }
    
}
