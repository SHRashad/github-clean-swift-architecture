//
//  RepositoryListViewModel.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import Foundation
import domain
import RxSwift

class RepositoryListViewModel : PaginatableViewModel{
    var paging: PagingEntity = PagingEntity(pageNum: 1, perPage: 10)
    
    var hasNext: Bool = true
    
    var isCurrentlyFetchingData: Bool = false
    
   
    
    var disposeBag = DisposeBag()
    let getRepositoryListUseCase: GetRepositoryListUseCase
    
    var repositorySubjects: BehaviorSubject<[RepositoryEntity]?> = BehaviorSubject(value: nil)
    
    private var repositoryModels: [RepositoryEntity] {
        get {
            return (try? repositorySubjects.value()) ?? []
        }
        
        set {
            repositorySubjects.onNext(newValue)
        }
    }
    
    var filterBody: FilterBodyEntity = FilterBodyEntity() {
        didSet {
            var mutatingSelf = self
            mutatingSelf.reset()
            mutatingSelf.repositoryModels.removeAll()
            
            self.fetchRepositoryList()
        }
    }
    
    init(getRepositoryListUseCase: GetRepositoryListUseCase) {
        self.getRepositoryListUseCase = getRepositoryListUseCase
    }
    
    
    func fetchRepositoryList() {
        self.isCurrentlyFetchingData = true
        _ = getRepositoryListUseCase.execute(input: (paging, filterBody))
            .do(onNext: { response in
                self.isCurrentlyFetchingData = false
                self.repositoryModels.append(contentsOf:response.items )
                self.hasNext = (self.paging.totalCount < 10000) && (self.paging.totalCount < response.totalCount)
            })
            .subscribe().disposed(by: self.disposeBag)
    }
    
    func loadMore() {
        fetchRepositoryList()
    }
    
    func getItem(at index: Int) -> RepositoryEntity? {
        if index < repositoryModels.count  {
            return repositoryModels[index]
        }
        return nil
    }
    
    func getSortItems() ->[SortItem<RepoSortType>] {
        let selected = filterBody.sortBy
        let values : [RepoSortType] = [.byForks, .byStars,.byUpdated]
        let items = values.map({SortItem(item: $0, state: ($0 == selected))})
        return items
    }
}
