//
//  RepositoryListController.swift
//  presentation
//
//  Created by Rashad Shirizada on 25.01.22.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class RepositoryListController: BaseController {
    var disposeBag = DisposeBag()
    
    private lazy var searchbar: CustomSearchbar = {
        let searchbar = CustomSearchbar(bgColor: .clear)
        
        return searchbar
    }()
    
    private lazy var filterButton: FilterButton = {
        let filterButton = FilterButton()
        filterButton.setImage(UIImage(named: "filter"), for: .normal)
        return filterButton
    }()
    
    private lazy var topView: UIView = {
        let view = UIView()
        
        view.addSubview(searchbar)
        view.addSubview(filterButton)
        
        searchbar.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview().offset(4)
            make.bottom.equalToSuperview().offset(-4)
            make.trailing.equalTo(filterButton.snp.leading).offset(-20)
            make.height.equalTo(44)
            make.centerY.equalToSuperview()
        }
        
        filterButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.width.height.equalTo(40)
            make.centerY.equalTo(searchbar.snp.centerY)
        }
        
        return view
    }()
    
    private lazy var repositoryTableView: UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.registerCell(cell: RepositoryTableCell.self)
        table.enablePagination(viewmodel: self.repositoryListViewModel).disposed(by: self.disposeBag)
        return table
    }()
    
    var repoCellEvent: PublishSubject<RepositoryCellEvent> = PublishSubject()
    var onFilter: (()->()) = {}
    var onItem: ((IndexPath)->()) = {_ in }
    let repositoryListViewModel: RepositoryListViewModel
    
    init(repositoryListViewModel: RepositoryListViewModel)  {
        self.repositoryListViewModel = repositoryListViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        repositoryListViewModel.fetchRepositoryList()
    }
    
    override func handleEvents() {
        repositoryListViewModel.repositorySubjects.map({$0 ?? []}).bind(to: self.repositoryTableView.rx.items(cellIdentifier: RepositoryTableCell.reuseIdentifier, cellType: RepositoryTableCell.self)) { [weak self] row, data , cell in
            
            guard let self = self else { return }
            cell.setup(repository: data, event: self.repoCellEvent)
            
        }.disposed(by: self.disposeBag)
        
        self.searchbar.rx.text
            .orEmpty
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .skip(1)
            .do(onNext: {[weak self] key in
                
                self?.repositoryListViewModel.filterBody.searchKey = key
                
            }).subscribe().disposed(by: self.disposeBag)
        
        self.filterButton.onTap = {[weak self] in
            self?.onFilter()
        }
        
        self.repositoryTableView.rx.itemSelected
            .do(onNext: {[weak self] indexPath in
                self?.onItem(indexPath)
            }).subscribe().disposed(by: self.disposeBag)
        
    }
    
    override func setupView() {
        super.setupView()
        
        view.addSubview(topView)
        view.addSubview(repositoryTableView)
        
        topView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        repositoryTableView.snp.makeConstraints { make in
            make.top.equalTo(topView.snp.bottom).offset(20)
            make.leading.equalTo(topView.snp.leading)
            make.trailing.equalTo(topView.snp.trailing)
            make.bottom.equalToSuperview().offset(-20)
        }
    }
    
}
