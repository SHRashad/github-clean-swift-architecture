//
//  SorController.swift
//  presentation
//
//  Created by Rashad Shirizada on 26.01.22.
//

import UIKit
import RxSwift

class SortController<T: SortItemProtocol>: BasePopupController {
    var disposeBag = DisposeBag()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = pageTitle
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return label
    }()
    
    private var itemsStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 10
        return stack
    }()
    
    private lazy var submitButton : BaseButton = {
        let button = BaseButton()
        button.setTitle("Submit", for: .normal)
        button.backgroundColor = .blue
        button.layer.cornerRadius = 10
        return button
    }()
    
    private lazy var resetButton : BaseButton = {
        let button = BaseButton()
        button.setTitle("Reset", for: .normal)
        button.backgroundColor = .red
        button.layer.cornerRadius = 10
        return button
    }()
    
    private lazy var buttonsStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 20
        stack.distribution = .fillEqually
        
        stack.addArrangedSubview(submitButton)
        stack.addArrangedSubview(resetButton)
        return stack
    }()
    
    var pageTitle:String?
    var sortItems: [(SortItem<T>, BehaviorSubject<Bool>)]
    var previouslySelectedItem: (SortItem<T>, BehaviorSubject<Bool>)?
 
    var onFilter: PublishSubject<[SortItem<T>]> = PublishSubject()
    init(title:String?, items: [SortItem<T>]) {
        self.pageTitle = title
        self.sortItems = items.map({($0, BehaviorSubject(value: $0.state))})
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupItems()
    }
    
    override func handleEvents() {
        self.resetButton.onTap = { [ weak self] in
            guard let self = self else { return }
            
            self.previouslySelectedItem?.1.onNext(false)
        }
        
        self.submitButton.onTap = { [ weak self] in
            guard let self = self else { return }
            let values = self.sortItems.map({ item -> SortItem<T> in
                var item = item
                let value = (try? item.1.value()) ?? false
                
                item.0.state = value
                return item.0
            })
            
            self.onFilter.onNext(values)
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func setupItems()  {
        self.sortItems.forEach { [weak self] item in
            guard let self = self else { return }
            let itemView = self.getItemView(item: item.0.item, state: item.1)
            self.itemsStack.addArrangedSubview(itemView)
            
            if item.0.state {
                self.previouslySelectedItem = item
            }
            
            itemView.addTapGesture {
                self.previouslySelectedItem?.1.onNext(false)
                item.1.onNext(true)
                self.previouslySelectedItem = item
            }
        }
    }
    
    
    private func getItemView(item: SortItemProtocol, state: BehaviorSubject<Bool>) -> UILabel {
        let label = UILabel()
        label.text = item.name
        let font = state.map({$0 ? UIFont.systemFont(ofSize: 30, weight: .bold) : UIFont.systemFont(ofSize: 30, weight: .light)})
        font.bind(to: label.rx.font).disposed(by: self.disposeBag)
        return label
    }
    
    
    override func setupView() {
        super.setupView()
        addTitleLabel()
        addItemsStack()
        addButtonsStack()
    }
    
    private func addTitleLabel() {
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(14)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            
        }
    }
    
    private func addItemsStack () {
        containerView.addSubview(itemsStack)
        itemsStack.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
    }
    
    private func addButtonsStack() {
        containerView.addSubview(buttonsStack)
        buttonsStack.snp.makeConstraints { make in
            make.top.equalTo(itemsStack.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(80)
            make.trailing.equalToSuperview().offset(-80)
            make.height.equalTo(44)
            make.bottom.equalToSuperview().offset(-40)
        }
    }
}
