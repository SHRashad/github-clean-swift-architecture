//
//  RepositoryDetailViewModel .swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import Foundation
import domain
import RxSwift

class RepositoryDetailViewModel {
    //you can ask why i used these subjects, because data is instance. I consider all cases, if you change implementation to fetching from api , you need binding in order to set data after receiving. 
    var fullName: PublishSubject<String?> = PublishSubject()
    var desc: PublishSubject<String?> = PublishSubject()
    var creationDate: PublishSubject<Date?> = PublishSubject()
    var pushedDate: PublishSubject<Date?> = PublishSubject()
    var language: PublishSubject<String?> = PublishSubject()
    var defaultBranch: PublishSubject<String?> = PublishSubject()
    var visibility: PublishSubject<String?> = PublishSubject()
    
    
    let repository: RepositoryEntity
    var repositoryOwnerViewModel: RepositoryOwnerViewModel?
    
    init(repository: RepositoryEntity)  {
        self.repository = repository
        if let owner = repository.owner {
            self.repositoryOwnerViewModel = RepositoryOwnerViewModel(owner: owner)
        }
    }
    
    func loadData() {
        self.fullName.onNext(repository.fullName)
        self.desc.onNext(repository.description)
        self.creationDate.onNext(repository.createdAt)
        self.pushedDate.onNext(repository.pushedAt)
        self.language.onNext(repository.language)
        self.defaultBranch.onNext(repository.defaultBranch)
        self.visibility.onNext(repository.visibility)
    }
    
}
