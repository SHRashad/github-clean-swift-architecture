//
//  RepositoryDetailsController.swift
//  presentation
//
//  Created by Rashad Shirizada on 27.01.22.
//

import UIKit
import RxSwift

class RepositoryDetailsController: BaseController {
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var creationDateLbl: UILabel!
    @IBOutlet weak var pushedDateLbl: UILabel!
    @IBOutlet weak var langLbl: UILabel!
    @IBOutlet weak var defaultBranchLbl: UILabel!
    @IBOutlet weak var visibilityLbl: UILabel!
    @IBOutlet weak var htmlButton: LinkButton!
    @IBOutlet weak var eventsButton: LinkButton!
    @IBOutlet weak var downloadButton: LinkButton!
    @IBOutlet weak var userIdLbl: UILabel!
    @IBOutlet weak var userLoginLbl: UILabel!
    @IBOutlet weak var userTypeLbl: UILabel!
    
    let repositoryDetailViewModel: RepositoryDetailViewModel
 
    init(repositoryDetailViewModel: RepositoryDetailViewModel) {
        self.repositoryDetailViewModel = repositoryDetailViewModel
        super.init(nibName: nil, bundle: Bundle(for: RepositoryDetailsController.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDetails()
        
        self.repositoryDetailViewModel.loadData()
        self.repositoryDetailViewModel.repositoryOwnerViewModel?.loadData()

    }
    
    override func handleEvents() {
        htmlButton.onTap = {[weak self] in
            guard let self = self ,
                let url = self.repositoryDetailViewModel.repository.htmlUrlStr?.url
            else { return }
            DeeplinkManager.shared.openInSafari(url: url, from: self)
        }
        
        eventsButton.onTap = {[weak self] in
            guard let self = self ,
                let url = self.repositoryDetailViewModel.repository.eventsUrlStr?.url
            else { return }
            DeeplinkManager.shared.openInSafari(url: url, from: self)
        }
        
        downloadButton.onTap = {[weak self] in
            guard let self = self ,
                let url = self.repositoryDetailViewModel.repository.downloadUrlStr?.url
            else { return }
            DeeplinkManager.shared.openInSafari(url: url, from: self)
        }
    }
    
    private func setupDetails() {
        repositoryDetailViewModel.fullName.bind(to: self.fullNameLbl.rx.text).disposed(by: self.disposeBag)
        repositoryDetailViewModel.desc.bind(to: self.descLbl.rx.text).disposed(by: self.disposeBag)
        repositoryDetailViewModel.creationDate.compactMap({$0}).compactMap({DateFormatter.repositoryDate.string(from:$0)}).bind(to: creationDateLbl.rx.text).disposed(by: self.disposeBag)
        repositoryDetailViewModel.pushedDate.compactMap({$0}).compactMap({DateFormatter.repositoryDate.string(from:$0)}).bind(to: pushedDateLbl.rx.text).disposed(by: self.disposeBag)
        repositoryDetailViewModel.language.bind(to: self.langLbl.rx.text).disposed(by: self.disposeBag)
        repositoryDetailViewModel.defaultBranch.bind(to: self.defaultBranchLbl.rx.text).disposed(by: self.disposeBag)
        repositoryDetailViewModel.visibility.bind(to: self.visibilityLbl.rx.text).disposed(by: self.disposeBag)
        
        let ownerVm = repositoryDetailViewModel.repositoryOwnerViewModel
        ownerVm?.id.map({"\($0 ?? 0)"}).bind(to: self.userIdLbl.rx.text).disposed(by: self.disposeBag)
        ownerVm?.login.bind(to: self.userLoginLbl.rx.text).disposed(by: self.disposeBag)
        ownerVm?.type.bind(to: userTypeLbl.rx.text).disposed(by: self.disposeBag)
    }
    
}
